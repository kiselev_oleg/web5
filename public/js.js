/*global alert, prompt*/

function addThing() {
    if (document.getElementById("name").innerHTML === "!Неизвестно имя:") {
        return;
    }

    let name = document.getElementsByName("name")[0].value;
    let cost = document.getElementsByName("cost")[0].value;
    let number = document.getElementsByName("number")[0].value;

    if (null === name.match(/^[\w,а-яб,А-Я]{1,}$/)) {
        alert("неправильно указано имя");
        return;
    }
    if (null === cost.match(/^\d{1,}(.\d{1,}){0,1}$/)) {
        alert("неправильно указана цена");
        return;
    }
    if (null === number.match(/^\d{1,}$/)) {
        alert("неправильно указано количество");
        return;
    }

    document.getElementById("result").insertAdjacentHTML("beforeBegin", `
        <tr>
            <td>` + name + `</td>
            <td>` + cost + `</td>
            <td>` + number + `</td>
            <td>` + parseFloat(cost) * parseInt(number) + `</td>
        </tr>`);

    document.getElementById("maincost").innerHTML = String(
        parseFloat(document.getElementById("maincost").innerHTML) +
        parseFloat(cost) * parseInt(number)
    );
}

function hide() {
    let b = document.getElementById("hide");
    let tr = document.getElementById("table").getElementsByTagName("tr");
    let i;
    if (b.value === "скрыть") {
        b.value = "открыть";

        for (i = 1; i < tr.length - 1; i += 1) {
            tr[i].style.display = "none";
        }
    } else {
        b.value = "скрыть";

        for (i = 1; i < tr.length - 1; i += 1) {
            tr[i].style.display = "table-row";
        }
    }
}


function name() {
    if (document.getElementById("name").innerHTML !== "!Неизвестно имя:") {
        return;
    }
    let n = prompt("Для использования калькулятора введите имя ");
    if (n === null) {
        return;
    }
    if (null === n.match(/^[\w,а-яб,А-Я]{1,}$/)) {
        alert("ввведено некоректное имя");
        return;
    }
    document.getElementById("name").innerHTML = String(n + ":");
    document.getElementById("name").style.background = "#eee";
}

window.addEventListener("DOMContentLoaded", function () {
    document.getElementById("add").addEventListener("click", name);
    document.getElementById("add").addEventListener("click", addThing);
    document.getElementById("hide").addEventListener("click", hide);

    name();
});
